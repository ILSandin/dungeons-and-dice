class AddBoardIdToGame < ActiveRecord::Migration[7.1]
  def change
    add_column :games, :board_id, :integer
  end
end
