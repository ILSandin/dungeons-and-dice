class RelocateGameState < ActiveRecord::Migration[7.1]
  def change
    remove_column :games, :state
    add_column :game_users, :board_state, :text
  end
end
