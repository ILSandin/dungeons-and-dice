class CreateGames < ActiveRecord::Migration[7.1]
  def change
    create_table :games do |t|
      t.integer :game_status
      t.integer :turn_status
      t.integer :current_player_id
      t.string :title
      t.text :state

      t.timestamps
    end
  end
end
