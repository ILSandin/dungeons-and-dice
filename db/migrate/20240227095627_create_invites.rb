class CreateInvites < ActiveRecord::Migration[7.1]
  def change
    create_table :invites do |t|
      t.string :invite_code, null: false
      t.integer :user_id
      t.datetime :expires_at, null: false

      t.timestamps

      t.index :invite_code, name: :index_invite_hash, unique: true
    end
  end
end
