class AddNullConstraintsToGame < ActiveRecord::Migration[7.1]
  def change

    Game.all.each do |g|
      g.game_status = 0
      g.board_id = 1
      g.visiblity = 0
      g.save!
    end

    change_column_null :games, :game_status, false
    change_column_null :games, :board_id, false
    change_column_null :games, :visiblity, false
  end
end
