# This file should ensure the existence of records required to run the application in every environment (production,
# development, test). The code here should be idempotent so that it can be executed at any point in every environment.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Example:
#
#   ["Action", "Comedy", "Drama", "Horror"].each do |genre_name|
#     MovieGenre.find_or_create_by!(name: genre_name)
#   end

Board.find_or_create_by(id: 1) do |board|
  board.json = Boards::BoardOne.generate.board_json
  board.title = "Annoyed Animals"
  board.save
end

role_file = Rails.root.join('db', 'seeds', 'roles.yml')
roles = YAML::load_file(role_file)
roles.each do |role|
  row = Role.find_or_create_by(name: role["name"])
  row.description = role["description"]
  row.save
end
