Rails.application.routes.draw do
  devise_for :users, controllers: {
    registrations: 'users/registrations'
  }
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  # get 'board/show'
  get 'boards/show', to: 'board#list'
  get 'boards/show/:id', to: 'board#show', as: "board"

  namespace :users do
    get 'manage', to: 'manage#show'
    get 'manage/edit/:id', to: "manage#edit"
    patch 'manage/update'
  end

  namespace :game do
    get 'list'
    post 'join/:id', action: :join, as: "join"
    post 'leave/:id', action: :leave, as: "leave"
    get 'new'
    post 'create'
    get 'play/:id', action: :play
  end

  mount ActionCable.server, at: '/cable'

  # Defines the root path route ("/")
  root "home#index"
end
