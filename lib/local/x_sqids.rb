module Local
  class XSqids < Sqids
    def encode_number(number)
      encode([number])
    end

    def decode_number(hash)
      ret = decode(hash)
      if ret.length != 1
        raise ArgumentError, "Hash contains multiple elements"
      end
      ret[0]
    end
  end
end
