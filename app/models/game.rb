class Game < ApplicationRecord
  include SqidLink

  has_many :game_users
  has_many :users, through: :game_users
  belongs_to :board

  validates :board_id, :title, presence: true

  enum :game_status, [ :pending, :in_progress, :complete ], default: :pending
  enum :visiblity, [ :visible, :hidden ], default: :visible

  scope :joinable, -> { where(:game_status => :pending) }
  scope :running, -> { where(:game_status => :in_progress) }
  scope :visible, -> { where(:visiblity => :visible) }
  scope :browseable, -> { visible.joinable }
  scope :joined, ->(user) { select { |game| user.joined?(game) } }
  scope :not_joined, ->(user) { select { |game| !user.joined?(game) } }

  def add_player(player)
    self.user_ids = self.user_ids.push(player.id).uniq
    player_state = self.game_users.find_by(user_id: player.id)
    player_state.board = JSON.parse(self.board.json)
    player_state.save
  end

  def add_players(players)
    players.each {|player| add_player(player)}
  end

  def remove_player(player)
    self.user_ids = self.user_ids - [player.id]
  end

  def remove_players(players)
    players.each {|player| add_player(player)}
  end
end
