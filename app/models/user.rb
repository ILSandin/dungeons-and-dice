class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         #:recoverable,
         :rememberable, :validatable

  has_and_belongs_to_many :games, join_table: :game_users
  has_and_belongs_to_many :roles, join_table: :user_roles
  has_one :invite

  def can_join?(game)
    !joined?(game)
  end

  def joined?(game)
    games.include?(game)
  end

  def has_role?(role)
    role = role.to_s
    !roles.find {|r| r.name == role}.nil?
  end

  def add_role(role)
    r = Role.find_by(name: role)
    raise ArgumentError, "No such role #{role}" if r.nil?
    role_ids = role_ids.push(r.id).uniq
  end

  def add_roles(roles)
    roles.each {|role| add_roles(role)}
  end

  def remove_role(role)
    r = Role.find_by(name: role)
    raise ArgumentError, "No such role #{role}" if r.nil?
    role_ids = role_ids - [r.id]
  end

  def remove_roles(roles)
    roles.each {|role| remove_role(role)}
  end
end
