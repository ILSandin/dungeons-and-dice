class Invite < ApplicationRecord
  validates :invite_code, presence: true, uniqueness: true
  validates :expires_at, presence: true
  belongs_to :user, optional: true

  def self.generate(expires_at: 7.days.from_now)
    invite = self.new
    xsqids = Local::XSqids.new(
      min_length: Rails.configuration.x.sqids.min_length,
      alphabet: Rails.configuration.x.sqids.salt
    )
    invite.invite_code = xsqids.encode_number(DateTime.now.to_i)
    invite.expires_at = expires_at
    invite.user_id = nil
    invite.save!
    invite.invite_code
  end

  def expire!
    expires_at = DateTime.now
    save!
  end

  def is_valid?
    expires_at > DateTime.now
  end

  def is_used?
    !user_id.nil?
  end
end
