module SqidLink
  extend ActiveSupport::Concern

  def self.xsqids
    if @xsqids.nil?
      @xsqids = Local::XSqids.new(
        min_length: Rails.configuration.x.sqids.min_length,
        alphabet: Rails.configuration.x.sqids.salt
      )
    end
    @xsqids
  end

  included do
    def hashed_id
      SqidLink.xsqids.encode_number(id || 0)
    end

    def to_param
      hashed_id
    end
  end

  class_methods do
    def find_by_hash(hash)
      id = SqidLink.xsqids.decode_number(hash)
      find(id)
    end

    def find(*ids)
      search_ids = ids.flatten.compact
      if search_ids.length > 1
        search_ids.map! { |id| int_id_from_generic_id(id) }.uniq!
        search_ids.map do |id|
          super(id)
        end
      else
        super(int_id_from_generic_id(ids.first))
      end
    end

    private

    def int_id_from_generic_id(id)
      if id.is_a? Integer
        return id
      end
      if id.is_a? String
        if id.to_i.to_s == id
          return id
        else
          return SqidLink.xsqids.decode_number(id)
        end
      end
      nil
    end
  end
end
