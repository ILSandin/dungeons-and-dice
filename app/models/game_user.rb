class GameUser < ApplicationRecord
  belongs_to :game
  belongs_to :user

  attr_reader :state_hash

  STATE_KEYS = [:score, :resources, :board]

  after_initialize do |game_state|
    begin
      state_json = game_state.board_state || ''
      @state_hash = JSON.parse(state_json)
    rescue JSON::ParserError
      @state_hash = {}
      STATE_KEYS.each do |key|
        @state_hash[key.to_s] = {}
      end
    end
  end

  before_save do |game_state|
    game_state.board_state = JSON.generate(@state_hash)
  end

  STATE_KEYS.each do |method|
    define_method "#{method}" do
      @state_hash[method.to_s]
    end

    define_method "#{method}=" do |new_value|
      @state_hash[method.to_s] = new_value
    end
  end
end
