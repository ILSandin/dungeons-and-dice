module BoardSetupHelper
  class Board
    attr_accessor :board

    def self.define(&block)
      board_helper = BoardSetupHelper::Board.new
      board_helper.instance_eval(&block)
      board_helper
    end

    def initialize
      @board = []
    end

    def board_json
      JSON.generate(@board)
    end

    def add(value, neighbors = -1, hp: 1, x:, y:, w: 1, h:1, **args)
      id = @board.size

      if neighbors.is_a? Integer
        neighbors = [id + neighbors]
      end

      state = 'unexplored'
      if args[:tag] == :west_start || args[:tag] == :east_start
        state = 'starting'
      end

      node = {
        :id => id, :value => value,
        :state => state, :neighbors => neighbors,
        :hp => hp, :x => x, :y => y,  :w => w,  :h => h
      }

      [:fist, :treasure, :vp, :monster, :to_neighbor].each do |key|
        unless args[key].nil?
          node[key] = args[key]
          next if key == :to_neighbor
          node[:state] = key.to_s
        end
      end

      @board.append(node)

      neighbors.each do |nid|
        @board[nid][:neighbors].append(id)
      end
    end
  end
end
