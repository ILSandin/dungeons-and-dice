module GameHelper
  include ActionView::Helpers::FormOptionsHelper

  def board_title_form_elements(board)
    if board.nil?
      return board_form_elements
    else
      return board_from_url_elements(board)
    end
  end

  def join_button(game)
    if current_user.can_join?(game)
      link_button game_join_path(game), "Join", "user-plus", method: "post"
    elsif current_user.joined?(game)
      link_button game_path(game), "Resume", "dice"
    end
  end

  def leave_button(game, pending_only:false)
    if current_user.joined?(game) && !(!pending_only && game.pending)
      link_button game_leave_path(game), "Leave", "user-minus", method: "post"
    end
  end

  private
  def board_from_url_elements(board)
    out = ''
    out << "Board: "
    out << board.title
    out << '<input name="board[id]" type="hidden" value="'
    out << board.id.to_s
    out << '">'
    out.html_safe
  end

  def board_form_elements
    out = ''
    out << "Board: "
    out << select(:board, :id, Board.all.map {|b| [ b.title, b.id ]})
    out.html_safe
  end
end
