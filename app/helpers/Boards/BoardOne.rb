module Boards
  module BoardOne
    def self.generate
      BoardSetupHelper::Board.define do
        # Top left starting block
        add 2, [], x: 3.5, y: 1,    tag: :west_start
        add 9,     x: 3.5, y: 2,    tag: :west_start
        add 6,     x: 3.5, y: 3,    tag: :west_start
        add 7,     x: 2.5, y: 3.5,  tag: :west_start
        add 3,     x: 1.5, y: 4,    tag: :west_start
        add 12,    x: 0.5, y: 4,    tag: :west_start #5

        #Top right starting block
        add 4, [],  x: 14,   y: 1.5,  tag: :east_start
        add 5,      x: 14.5, y: 2.5,  tag: :east_start
        add 8,      x: 14.5, y: 3.5,  tag: :east_start
        add 10,     x: 14,   y: 4.5,  tag: :east_start
        add 11,     x: 14,   y: 5.5,  tag: :east_start #10

        #Purple Pup stub
        add 4, [0, 1], x: 4.5, y: 1.5,         to_neighbor: 12
        add [11],      x: 5.5, y: 1, w:2, h:2, monster: 'Purple Pup', hp: 4
        add 10,        x: 7.5, y: 0.5,         doubles: true, fist: true #13

        #Green Growler path
        add 10, [2, 3],               x: 3.5,  y: 4,               to_neighbor: 25
        add [2,4,6,8,10,12], [2, 14], x: 4.5,  y: 3.5,             doubles: true
        add 7, [12, 15],              x: 5.5,  y: 3,               to_neighbor: 12
        add 3, [12, 16],              x: 6.5,  y: 3
        add 10,                       x: 7.5,  y: 3.5
        add 12,                       x: 8,    y: 2,        h:1.5, vp: 3
        add 9, -2,                    x: 8.5,  y: 4
        add 6,                        x: 9.5,  y: 4
        add 6,                        x: 10.5, y: 3.5
        add 2,                        x: 9.5,  y: 2.5, w:1.5,      vp: 3
        add 12,                       x: 9.5,  y: 1.5,             doubles: true, fist: true
        add [5], -3,                  x: 11.5, y: 2.5, w:2, h:2,   monster: 'Green Growler', hp: 4
        add 3, [6, 25],               x: 13,   y: 1.5,             to_neighbor: 25
        add 9, [7, 8, 25],            x: 13.5, y: 3,               to_neighbor: 25 #27

        #2/12 stubb
        add 5, [4, 5],   x:1,   y:5
        add 6, [5, 27],  x:2,   y:5,        to_neighbor: 25
        add 4, [27, 28], x:1.5, y:6,        doubles: true, fist: true
        add 12, [29],    x:1,   y:7, h:1.5, vp: 3
        add 2, [29,30],  x:2,   y:7, h:1.5, vp: 3 #32

        #Grey Hound west/east
        add [8], [14, 28],   x:3,  y:5, w:2, h:2,   monster: 'Grey Hound', hp: 4
        add 9,               x:5,  y:5.5
        add 11,              x:6,  y:4.5,    h:1.5, treasure:true
        add 12,              x:7,  y:5,             vp: 3
        add 3, [20, 36],     x:8,  y:5,             to_neighbor: 38
        add [],              x:8,  y:6, w:4, h:3,   monster: 'Beefy Bearpion', hp: 12
        add 11, [21, 38],    x:10, y:5,             to_neighbor: 38
        add [2,4,6,8,10,12], x:11, y:4.75,          doubles: true
        add 7, [25, 40],     x:12, y:4.5
        add 2, [9, 10, 41],  x:13, y:5,      h:1.5, vp: 3 #42

        #West boss stub
        add [2,4,6,8,10,12], [34, 35], x:6, y:6, doubles: true
        add 10,                        x:6, y:7
        add 5, [38, 44],               x:7, y:7, to_neighbor: 38 #45

        #East boss stub
        add 4, [38],      x:12, y:7.75,             to_neighbor: 38
        add 3,            x:13, y:6.75,      h:1.5, treasure:true
        add 12, [10, 47], x:14, y:6.5,  w:2,        vp: 3 #48

        #Grey Hound south stub
        add 5, [33],         x:3.5, y:7
        add 7,               x:4.5, y:7.5
        add [2,4,6,8,10,12], x:4,   y:8.5,           doubles: true
        add 2,               x:3,   y:9,      h:1.5, vp: 3
        add 6,               x:2,   y:9.5,           doubles: true, fist: true
        add 5, -2,           x:3.5, y:10.5,          to_neighbor: 55
        add [4],             x:4.5, y:10, w:2, h:2,  monster: 'White Wolf', hp: 5 #55

        #Rabbit path
        add 6, [51, 55],               x:5,    y:9
        add 8, [55, 56],               x:6,    y:9,             to_neighbor: 55
        add 7,                         x:7,    y:9.5
        add [2,12],                    x:8,    y:9.5, w:2, h:2, monster: 'Primal Hare', hp: 5
        add 8, [38, 59],               x:10,   y:9,             doubles: true, fist: true, to_neighbor: 38
        add 6, [59, 60],               x:10,   y:10
        add 10, [60, 61],              x:11,   y:9.5
        add [3,11],                    x:12,   y:10,  w:2, h:2, monster: 'Punk Hare', hp: 5
        add 9, [62, 63],               x:12,   y:9
        add 7, [46, 64],               x:13,   y:8.5
        add 9,                         x:14,   y:8.5
        add 8, [63, 66],               x:14,   y:9.5
        add 2,                         x:15,   y:10,            doubles: true, fist: true
        add [2,4,6,8,10,12], [48, 66], x:14.5, y:7.5,           doubles: true #69
      end
    end
  end
end
