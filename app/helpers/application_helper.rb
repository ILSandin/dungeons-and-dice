module ApplicationHelper
  def link_button(link, text, icon=nil, method: :get)
    out = ''
    out << "<p><a class='btn btn-outline-secondary' data-method='"
    out << method.to_s
    out << "' href='"
    out << link
    out <<"'>"
    out << text
    if !icon.nil?
      out << " <i aria-hidden='true' class='fa-solid fa-fw fa-"
      out << icon
      out << "'></i>"
    end
    out << "</a></p>"
    out.html_safe
  end

  def button_button(text, icon=nil, type: "submit")
    out = ''
    out << "<button class='btn btn-outline-secondary' type='"
    out << type
    out << "'>"
    out << text
    if !icon.nil?
      out << " <i aria-hidden='true' class='fa-solid fa-fw fa-"
      out << icon
      out << "'></i>"
    end
    out << "</button>"
    out.html_safe
  end
end
