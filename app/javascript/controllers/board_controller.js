import { Controller } from "@hotwired/stimulus"
import { Board } from 'models/board'

// Connects to data-controller="board"
export default class extends Controller {
  async connect() {
    var board = new Board;
    board.set_target(jQuery("#svgarea"));
    await board.load_from_url("http://127.0.0.1:3000/boards/show/1.json");
    board.add_child_event_listener("mouseover", this.mouse_over_info);
    window.dice_globals ||= {};
    window.dice_globals.board = board;
  }

  mouse_over_info (e) {
    var target_element = $("#info_box")[0];
    var cell_id = e.target.attributes.cell_id.value;
    var cell_json = window.dice_globals.board.export_cell(cell_id);
    target_element.innerHTML = JSON.stringify(cell_json);
  }
}
