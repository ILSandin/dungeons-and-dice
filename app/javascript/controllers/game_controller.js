import { Controller } from "@hotwired/stimulus"
import { Board } from 'models/board'
import { createConsumer } from '@rails/actioncable'

// Connects to data-controller="game"
export default class extends Controller {
  static values = { gameId: String, boardId: Number }

  async connect() {
    var board = new Board;
    window.dice_globals ||= {};
    window.dice_globals.board = board;
    window.dice_globals.controller = this;
    board.set_target(jQuery("#svgarea"));

    window.dice_globals.channel = createConsumer().subscriptions.create({
      channel: "GameChannel",
      game_id: this.gameIdValue
    }, {
      received(data) { window.dice_globals.controller.dispatch(data) },
      connected() { window.dice_globals.controller.ws_connected() }
    });
  }

  ws_connected() {
    window.dice_globals.channel.send({
      command: "get_board_state",
      params: {}
    });
  }

  dispatch(data){
    switch(data['command']){
      case 'echo':
        console.log(data['params']);
        break;
      case 'get_board_state':
        this.load_from_socket(data['params']);
        break;
      default:
        console.log("Unknown method: " + data['command']);
    }
  }

  load_from_socket(json) {
    var board = window.dice_globals.board;
    board.load_from_json(json);
    board.hydrate();
    board.add_child_event_listener("click", this.mouse_click_info);
  }

  mouse_click_info(e) {
    var cell_id = e.target.attributes.cell_id.value;
    var cell_data = window.dice_globals.board.export_cell(cell_id);
    window.dice_globals.channel.send({
      command: "echo",
      params: {
        cell_data: cell_data
      }
    });
  }
}
