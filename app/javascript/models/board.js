import { FetchRequest } from '@rails/request.js'

export class Board {
  #cell_size = 58;

  constructor() {
    this._loaded = false;
    this._hydrated = false;
  }

  set_target(element) {
    this.svgarea = element;
  }

  async load_from_url(url) {
    const request =  new FetchRequest("get", url);
    const response = await request.perform();
    this._cells = await response.json;
    this._load();
  }

  load_from_json(json) {
    this._cells = json;
    this._load();
  }

  _load(){
    var t = this;
    $.each(this._cells, function(){
      t.render_cells(this);
    });
    this._loaded = true;
    this._hydrated = false;
  }

  hydrate() {
    if(!this._loaded) { return false };
    var t = this;
    $.each(this.cells, function(){
      var cell = this;
      cell.neighborRefs = [];
      $.each(cell.neighbors, function(){
        cell.neighborRefs.push(t.cells[this]);
      });
    });
    this._hydrated = true;
    return this._hydrated;
  }

  export_cell(id){
    var cell_data = { ...this._cells[id] };
    if(this._hydrated) {
      delete cell_data.neighborRefs;
    }
    return cell_data;
  }

  get children() {
    return this.svgarea.children('rect');
  }

  get cells() {
    return this._cells;
  }

  add_child_event_listener(event, callback) {
    $.each(this.children, function(){
        this.addEventListener(event, callback);
    });
  }

  clear_svg(){
    this.svgarea.empty();
  }

  render_cells(cell) {
    if(!this._loaded) {
      cell.x *= this.#cell_size;
      cell.y *= this.#cell_size;
      cell.w *= this.#cell_size;
      cell.h *= this.#cell_size;
    }
    var css_class = cell.state + "_cell";
    var el = $(document.createElementNS("http://www.w3.org/2000/svg", "rect")).attr({
        x: cell.x,
        y: cell.y,
        width: cell.w,
        height: cell.h,
        stroke:"black",
        "stroke-width":"2",
        class:css_class,
        id: "cell" + cell.id,
        label_id: "cell" + cell.id + "-label",
        cell_id: cell.id
    });
    this.svgarea.append(el);
    var text = $(document.createElementNS("http://www.w3.org/2000/svg", "text")).attr({
      x: parseFloat(el[0].attributes.x.value) + (this.#cell_size*(1/4)),
      y: parseFloat(el[0].attributes.y.value) + (this.#cell_size*(3/4)),
      fill: "Black",
      "pointer-events": "none",
      "font-size": this.#cell_size*(5/8),
      id: el[0].attributes.label_id.value
    });
    text[0].textContent = cell.value;
    if(text[0].textContent == "2,4,6,8,10,12"){
      text[0].textContent = "D";
    }
    this.svgarea.append(text);
  }
}