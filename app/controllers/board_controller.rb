class BoardController < ApplicationController
  before_action :load_board, except: [:list]

  def show
    if(params[:format] == 'json')
      render json: @board.json
      return
    end
  end

  def list
    @boards = Board.all
  end

  private
  def load_board
    @board = Board.find(params[:id])
  end
end
