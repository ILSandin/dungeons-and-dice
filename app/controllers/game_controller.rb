class GameController < ApplicationController
  before_action :check_user

  def new
    @board = Board.find_by(id: params[:board_id])
  end

  def create
    game =  Game.new
    game.board_id = params[:board][:id]
    game.title = params[:title]
    if game.save
      game.add_player(current_user)
      flash[:alert] = nil
      redirect_to action: "play", id: game.id
    else
      flash[:alert] = "Error: " + game.errors.full_messages.join(", ")
      render :new, status: :unprocessable_entity
    end
  end

  def join
    game = Game.find(params[:id])
    if current_user.can_join?(game)
      game.add_player(current_user)
      redirect_to action: "play", id: game.id
    else
      flash[:alert] = "Error: Can not join game"
      render :new, status: :unprocessable_entity
    end
  end

  def leave
    game = Game.find(params[:id])
    if current_user.joined?(game)
      game.remove_player(current_user)
      redirect_to root_path
    else
      flash[:alert] = "Error: Already unenrolled from this game"
      render :new, status: :unprocessable_entity
    end
  end

  def list
    @browseable = Game.browseable.not_joined(current_user)
    @joined = Game.joined(current_user)
  end

  def play
    @game = Game.find(params[:id])
    if !current_user.joined?(@game)
      flash[:alert] = "Error: Not enrolled in selected game"
      redirect_to root_path
    end
  end

  private

  def check_user
    if current_user.nil?
      flash[:alert] = "You do not have permission to access that page"
      redirect_to root_path
    end
  end
end
