# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    return unless new_validation?
    super
  end

  # POST /resource
  def create
    return unless create_validation?
    super
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:invite])
  end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  def after_sign_up_path_for(resource)
    @invite_code = params[:invite]
    invite = Invite.find_by(invite_code: @invite_code)
    invite.user = resource
    invite.save
    super(resource)
  end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end

  def valid_invite_code?
    @invite_code = params[:invite]
    invite = Invite.find_by(invite_code: @invite_code)
    return !invite.nil? && invite.is_valid? && !invite.is_used?
  end

  def new_validation?
    unless valid_invite_code?
      redirect_to :root, notice: "Invalid invite code"
      return false
    end
    true
  end

  def create_validation?
    unless valid_invite_code?
      flash.alert = "Invalid invite code"
      @user = User.new
      render :new, status: :unauthorized
      return false
    end
    true
  end

end
