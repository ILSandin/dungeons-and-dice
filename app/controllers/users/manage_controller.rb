class Users::ManageController < ApplicationController
  before_action :load_user, except: :show
  before_action :check_roles

  def show
    @users = User.all
  end

  def edit
  end

  def update
    @user.role_ids = params[:role].map {|r|r.to_i}.select {|r| r > 0}
    render :edit
  end

  private
  def load_user
    @user = User.find(params[:id])
  end

  def check_roles
    unless current_user.has_role?(:manage_users)
      flash[:alert] = "You do not have permission to access that page"
      redirect_to root_path
    end
  end
end
