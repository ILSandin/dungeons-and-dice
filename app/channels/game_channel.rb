class GameChannel < ApplicationCable::Channel
  def subscribed
    stream_from game_public_channel
    stream_from game_user_channel
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def receive(data)
    self.send(data['command'],data['params']);
  end

  private

  def game_public_channel
    params["game_id"]
  end

  def game_user_channel
    params["game_id"] + '-' + current_user.email
  end

  def echo(args)
    values = args['cell_data']
    ActionCable.server.broadcast(game_user_channel, {
      command: "echo",
      params: {
        cell_id: values["id"],
        message: "Responding to click in cell",
        client: current_user.email
      }
    })
  end

  def get_board_state(args)
    game = Game.find(params["game_id"])
    player_state = game.game_users.find_by(user_id: current_user.id)
    ActionCable.server.broadcast(game_user_channel, {
      command: "get_board_state",
      params: player_state.board
    })
  end
end
