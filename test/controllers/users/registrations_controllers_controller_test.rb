require "test_helper"

# require 'pry'

class Users::RegistrationsControllersControllerTest < ActionDispatch::IntegrationTest
  @@new_path = '/users/sign_up'
  @@create_path = '/users'
  @@good_invite ='supersekrit'
  @@bad_invite = 'lolnope'

  test "new should redirect with no invite code" do
    get @@new_path
    assert_response :redirect
  end

  test "new should redirect with bad invite code" do
    get @@new_path, params: {
      invite: @@bad_invite
    }
    assert_response :redirect
  end

  test "should get new with valid invite code" do
    get @@new_path, params: {
      invite: @@good_invite
    }
    assert_response :success
  end

  test "should fail on an invalid post" do
    post @@create_path, params: {
      user: {
        email: 'good@test.com',
        password: 'hunter2',
        password_confirmation: 'hunter2'
      },
      invite: @@bad_invite
    }

    assert_response :unauthorized
  end

  test "should succeed on valid post" do
    post @@create_path, params: {
      user: {
        email: 'good@test.com',
        password: 'hunter2',
        password_confirmation: 'hunter2'
      },
      invite: @@good_invite
    }

    assert_response 303
  end
end
