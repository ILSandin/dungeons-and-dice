require "test_helper"

class Users::ManageControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get users_manage_show_url
    assert_response :success
  end

  test "should get edit" do
    get users_manage_edit_url
    assert_response :success
  end

  test "should get update" do
    get users_manage_update_url
    assert_response :success
  end
end
